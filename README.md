## Title: Segment Shopping Customers

- Problem Statement: 

Understand the target customers for the marketing team to plan a strategy

- Context:

Your boss wants you to identify the most important shopping groups based on income, age, and the mall shopping score.
He want the ideal number of groups with a label for each

- Objective

Divide your mall target market into approachable groups.
Create subsets of a marrket based on demographics behioral criteria to better understand the target for marketing activities.

- The Approach
1- Perform some quic EDA(Exploratory Data Analysis)
2- Use KMEANS Clustering Algorithm to create our segments
3- Use summary statistics on the clusters
4- Visualize

- Requiremements

1- Standard Python Installation
2- Jupyter Notebook


